'use strict';

var express = require('express');
var EjercicioController = require('./ejercicios.controller');
var api = express.Router();

var multer  = require('multer')
var storage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
  }
})
var upload = multer({ dest:'/uploads',storage:storage })

api.post('/pdfdownload', EjercicioController.DownloadPdfEjercicio);
api.post('/pdfsend', EjercicioController.SendPdfEjercicio);
api.post('/solicitudsend',upload.single('foto'),EjercicioController.SendPdfSolicitud);
api.post('/solicitudsenddieta',upload.single('foto'),EjercicioController.SendPdfSolicitudDieta);

module.exports = api;
