var fs = require('fs-extra');
const moment = require("moment");
var options = {
  format: 'Letter',
  orientation: "landscape"
};
function setNameDay(i) {
  switch (i) {
    case 1:
      return 'Lunes';
    case 2:
      return 'Martes';
    case 3:
      return 'Miércoles';
    case 4:
      return 'Jueves';
    case 5:
      return 'Viernes';
    case 6:
      return 'Sábado';
    case 7:
      return 'Domingo';
    default:
      return 'Lunes';
  }
}
async function createPdfEjercicio(data) {
  console.log(data.foto);
  var content = `
    <style>
        body {font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
            -webkit-font-smoothing: antialiased;margin: 0px;}
        .container {margin: 0 2rem;}
        .header {background-color: #0156f2;background: radial-gradient(at top right,#040d20 30%,#015aff 100%);padding: 2rem;}
        .header .content {display: flex;align-items: center;justify-content: space-between;}
        .header p {color: white;}
        .header a {color: white;font-weight: bold;}
        .header img {width: 170px; margin-bottom: .5rem}
        .header .title {color: white;margin: 0px;}
         .header table td .title {color: white;margin: 0px;font-size: 28px}
        .header table td {width: 100%}
        .header table td.date{text-align: start;color: white}
        table.routine {border-collapse: collapse;width: 100%; margin: 2rem auto}
        table.routine td, table th {border: 1px solid #e5e5e5;text-align: center;vertical-align: center;}
        table.routine tr.title th{background-color: #023aa2;font-weight: bold;font-size: 14px;}
        table.routine th {background-color: #8e9ebd;color: white;text-transform: uppercase;padding: .5rem .2rem;border-color: #a0a2a4;font-size: 8px;}
        table.routine tr {background-color: #f2f2f2;}
        table.routine tr:nth-child(even){background-color: white;}
        table.routine td.index {font-weight: bold;width: 20px;}
        /*table td.sets, table td.rpe, table td.carga, table td.descanso {width: 40px;}*/
        table td.notas {width: 180px;}
        table td {font-size: 8px}
        
    </style>
     <div class="header">
        <div class="content">
            <table>
                <tr>
                    <td><h1 class="title">RUTINA PERSONALIZADA</h1></td>
                    <td><img src="file://${__dirname}/logoHeaderInv.svg" alt="" ></td>
                </tr>
                <tr>
                    <td class="date"><h1>Fecha: ${moment().format('DD/MM/YYYY')}</h1></td>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="container">
        
        
    `;
  for (let i = 0; i < data.length; i++) {
    console.log(data[i]);
    content = content.concat(
      `<table class="routine">
            <thead>
            <tr class="title">
                <th colspan="8">Dia ${setNameDay(data[i].id)}</th>
            </tr>
            <tr>
                <th></th>
                <th>Ejercicio</th>
                <th>N Series</th>
                <th>Repeticiones</th>
                <th>RPE/RIR</th>
                <th>Carga (Kg)</th>
                <th>Descanso</th>
                <th>Notas</th>
            </tr>
            </thead>
            <tbody>`
    );
    for (let j = 0; j < data[i].items.length; j++) {
      var item = data[i].items[j];
      content = content.concat(`
        <tr>
          <td class="index">${j+1}</td>
          <td style="text-align: start;margin: .5rem 0px">
            <div>
             <img src="${item.Small_Img_1}" alt="" >
                <b><a href="https://fullmusculo.com/ejercicios/#/ejercicio/${item.Exercise_Id}">${item.Exercise_Name_Complete}</a></b>
                <p style="font-size: 6px; margin-bottom: 0px"><b>Músculo:</b> ${item.Target_Muscle_Group}</p>
                <p style="font-size: 6px; margin-top: 0px"><b>Implementos:</b> ${item.Apparatus_Groups_Name}</p>
             </div>
          </td>
          <td class="sets">${item.sets}</td>
          <td>${item.reps}</td>
          <td class="rpe"></td>
          <td class="carga"></td>
          <td class="descanso"></td>
          <td class="notas"></td>
        </tr>`);
    }
    content = content.concat(`</tbody></table>`);
  }
  content = content.concat(`<div style="text-align: center; margin-top: 2rem">
            <p style="font-size: 8px;margin-bottom: 5px;margin-top: 0;">Si deseas tener un plan de ejercicio personalizado puedes utilizar la opción de plan personalizado de Fullmusculo en la web</p>
        </div>
    </div>`)
  return {
    content,
    options
  };
}

async function createSolicitud(data) {
  var content = `
  <style>
      body {font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
      -webkit-font-smoothing: antialiased;}
    </style>
    <div style="text-align: center">
      <h1>Solicitud de Plan Personalizado</h1>
    </div >
    <h2>Datos ingresados</h2>
    <p><b>Primer nombre: </b>${data.primer_nombre}</p>
    <p><b>Apellido: </b>${data.apellido}</p>
    <p><b>Email de contacto (Verificar que esté bien escrito): </b>${
      data.email
    }</p>
    <p><b>Cómo te enteraste de nuestra promoción?: </b>${
      data.como_te_enteraste
    }</p>
    <p><b>Si hiciste el pago desde la cuenta paypal de otra persona es importante que nos indiques su nombre y correo electrónico:</b>${
      data.paypal_otro
    }</p>
    <p><b>Sexo: </b>${data.sexo}</p>
    <p><b>Edad: </b>${data.edad}</p>
    <p><b>País y ciudad de donde nos escribes: </b>${data.pais}</p>
    <p><b>Altura en centímetros (cm): </b>${data.altura}</p>
    <p><b>Peso actual en Kilos (Kg): </b>${data.peso}</p>
    <p><b>Peso mínimo y máximo que has presentado en los últimos años: </b>${
      data.peso_min_max
    }</p>
    <p><b>Medidas de cintura y cadera en centímetros (cm): </b>${
      data.cintura_cadera
    }</p>
    <p><b>Porcentaje de grasa corporal (si lo sabe actualizado) y cómo fue medido?: </b>${
      data.grasa_corporal
    }</p>
    <p><b>Cantidad de agua que tomas al día (Indicar si en litros o vasos de agua): </b>${
      data.cantidad_agua
    }</p>
    <p><b>Fumas tabaco o eres fumador pasivo?: </b>${data.tabaco}</p>
    <p><b>Tomas café o té regularmente?: </b>${data.cafe_te[0] ? 'Té, ' : ''}${
    data.cafe_te[1] ? 'Café, ' : ''
  }${data.cafe_te[2] ? 'Ninguno' : ''}</p>
    <p><b>Cual es la frecuencia de evacuaciones? Cada cuanto vas al baño ? Sufres de estreñimiento?: </b>${
      data.evacuaciones
    }</p>
    <p><b>Sientes ansiedad en algún momento del día ? Si la respuesta es sí indicar a qué hora?: </b>${
      data.ansiedad
    }</p>
    <p><b>Describe un día típico para ti:: </b>${data.dia_tipico}</p>
    <p><b>¿Has entrenado anteriormente?: </b>${
      data.entrenamiento_anteriormente
    }</p>
    <p><b>Si la anterior pregunta fue si, ¿Desde hace cuanto tiempo entrenas (Indicar en meses o en años)?: </b>${
      data.entrenamiento_desde_cuando
    }</p>
    <p><b>Que tipo de entrenamiento realizas? Ser especifico: </b>${
      data.tipo_entrenamiento
    }</p>
    <p><b>Cual es tu experiencia (en años) con el entrenamiento de pesas ?: </b>${
      data.experiencia_pesas
    }</p>
    <p><b>A qué hora del día entrenas normalmente?: </b>${
      data.hora_entrenamiento
    }</p>
    <p><b>¿Cuantos días a la semana entrenas o dispones para entrenar? Sé realista: </b>${
      data.dias_semana_entrena
    }</p>
    <p><b>¿De cuanto tiempo dispones al día para entrenar? Sé realista: </b>${
      data.tiempo_para_entrenar
    }</p>
    <p><b>¿Objetivos y expectativas con nuestra asesoría?: </b>${
      data.objetivos_espectativas
    }</p>
    <p><b>Explicar con detalle cual es el objetivo que quieres lograr con nuestra asesoría: </b>${
      data.objetivo_detallado
    }</p>
    <p><b>Cuantas comida haces al día normalmente y en que horarios?: </b>${
      data.comidas_dia
    }</p>
    <p><b>Qué Frutas y vegetales consumes normalmente?: </b>${
      data.frutas_vegetales_consume
    }</p>
    <p><b>Qué alimentos nunca consumes porque no te gustan, te dan alergia o intolerancia?: </b>${
      data.alimento_no_consume
    }</p>
    <p><b>Desayuno de ayer: </b>${data.desayuno}</p>
    <p><b>Merienda de media mañana de ayer (Si la hubo): </b>${
      data.merienda_media_mañana
    }</p>
    <p><b>Almuerzo o comida de ayer: </b>${data.almuerzo}</p>
    <p><b>Merienda de media tarde de ayer (Si la hubo): </b>${
      data.merienda_media_tarde
    }</p>
    <p><b>Cena de ayer: </b>${data.cena}</p>
    <p><b>Merienda antes de dormir de ayer (Si la hubo): </b>${
      data.merienda_antes_dormir
    }</p>
    <p><b>¿Has tomado o tomas suplementos?: </b>${data.tomas_suplementos}</p>
    <p><b>Si la anterior respuesta fue si, dime cuales suplementos has tomado o tomas actualmente?: </b>${
      data.suplementos_que_toma
    }</p>
    <p><b>¿Tienes alguna Alergias/Intolerancias/Enfermedades/Medicación? Especificar: </b>${
      data.alergias
    }</p>
    <p><b>Le ha dicho su médico alguna vez que padece una enfermedad cardiaca y que sólo debe hacer aquella actividad física que le aconseje un médico?: </b>${
      data.enfermedad_cardiaca
    }</p>
    <p><b>Tiene dolor en el pecho cuando hace actividad física?: </b>${
      data.dolor_de_pecho
    }</p>
    <p><b>En el último mes, ¿ha tenido dolor en el pecho cuando no hacía actividad física?: </b>${
      data.dolor_de_pecho_sin_ejercicio
    }</p>
    <p><b>Pierde el equilibrio debido a mareos o se ha desmayado alguna vez?: </b>${
      data.pierde_equilibrio
    }</p>
    <p><b>Tiene problemas en huesos o articulaciones (por ejemplo, espalda, rodilla o cadera) que puedan empeorar si aumenta la actividad física?: </b>${
      data.problemas_huesos
    }</p>
    <p><b>Conoce alguna razón por la cual no debería realizar actividad física?: </b>${
      data.razon_no_ejercicio
    }</p>
    <p><b>¿Tienes algún impedimento físico o has sufrido alguna lesión que te limite o no te permita realizar ejercicio físico?: </b>${
      data.impedimento
    }</p>
    <p><b>Comentarios adicionales que desees agregar.: </b>${
      data.comentarios
    }</p>
    <p><b>Whatsapp: </b>${data.whatsapp}</p>
    `;

  return {
    content,
    options
  };
}
module.exports = {
  createPdfEjercicio,
  createSolicitud
};
