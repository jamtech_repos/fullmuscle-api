'use strict';
var PdfController = require('./pdf.controller');
var transporter = require('../mail');
var fs = require('fs-extra');
const path = require('path');
var pdf = require('html-pdf');

async function DownloadPdfEjercicio(req, res) {
  try {
    var data = req.body.data;
    var content = await PdfController.createPdfEjercicio(data);
    pdf
      .create(content.content, content.options)
      .toFile(
        path.join(__dirname, '..', 'assets/pdf/RutinaPersonal.pdf'),
        (err, response) => {
          console.log(response);
          if (err) {
            res.status(400).send({
              error: err
            });
          } else {
            res.sendFile(
              path.join(__dirname, '..', 'assets/pdf/RutinaPersonal.pdf')
            );
          }
        }
      );
  } catch (error) {
    console.log(error);
    res.status(500).send({
      error: error
    });
  }
}

async function SendPdfEjercicio(req, res) {
  try {
    var data = req.body.data;
    var content = await PdfController.createPdfEjercicio(data);
    pdf
      .create(content.content, content.options)
      .toFile(
        path.join(__dirname, '..', 'assets/pdf/RutinaPersonal.pdf'),
        (err, response) => {
          if (err) {
            res.status(400).send({
              error: err
            });
          } else {
            var mailOptions = {
              from: 'info@fullmusculo.com',
              to: req.query.email,
              subject: 'Rutina personal de ejercicios',
              html:
                '<p>Adjunto está el formulario con los datos y la información del usuario.</p>',
              attachments: [
                {
                  filename: 'RutinaPersonal.pdf',
                  path: path.join(
                    __dirname,
                    '..',
                    'assets/pdf/RutinaPersonal.pdf'
                  ),
                  contentType: 'application/pdf'
                }
              ]
            };
            transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log('error', error);
                res.status(500).send({
                  status: false
                });
              } else {
                console.log('bien');
                fs.remove(
                  path.join(
                    __dirname,
                    '..',
                    'assets/pdf/RutinaPersonal.pdf',
                    err => {
                      if (err) return console.error(err);
                      res.status(200).send({
                        status: true
                      });
                    }
                  )
                );
              }
            });
          }
        }
      );
  } catch (error) {
    console.log(error);
    res.status(500).send({
      error: error
    });
  }
}

async function SendPdfSolicitud(req, res) {
  try {
    var data = req.body;
    console.log(req.query.dieta);
    var content = await PdfController.createSolicitud(data);
    pdf
      .create(content.content, content.options)
      .toFile(
        path.join(
          __dirname,
          '..',
          'assets/pdf/Formulario' +
            data.primer_nombre +
            '' +
            data.apellido +
            '.pdf'
        ),
        (err, response) => {
          if (err) {
            res.status(400).send({
              error: err
            });
          } else {
            var mailOptions = {
              from: 'info@fullmusculo.com',
              to:
                req.query.dieta === 'true'
                  ? 'info@fullmusculo.com;nutricionista@fullmusculo.com'
                  : 'info@fullmusculo.com',
              /* to: 'utrera1695@gmail.com', */
              subject: 'Plan para ' + data.primer_nombre + ' ' + data.apellido,
              html:
                '<p><b>' +
                data.primer_nombre +
                ' ' +
                data.apellido +
                '</b> ha solicitado un plan personalizado de ejercicios.</p>' +
                '<p>' +
                (req.query.dieta === 'true'
                  ? 'Además, seleccionó el plan de dieta adicional'
                  : '') +
                '</p>' +
                '<p>El correo de este usuario es ' +
                data.email +
                '</p>' +
                '</hr></hr>' +
                '<p>Adjunto está el formulario con los datos y la información del usuario.</p>',
              attachments: [
                {
                  filename:
                    'Formulario' +
                    data.primer_nombre +
                    '' +
                    data.apellido +
                    '.pdf',
                  path: path.join(
                    __dirname,
                    '..',
                    'assets/pdf/Formulario' +
                      data.primer_nombre +
                      '' +
                      data.apellido +
                      '.pdf'
                  ),
                  contentType: 'application/pdf'
                },
                {
                  filename: req.file.originalname,
                  path: path.join(req.file.path),
                  contentType: req.file.mimetype
                }
              ]
            };
            transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log('error', error);
                res.status(500).send({
                  status: false
                });
              } else {
                console.log('bien');
                fs.remove(
                  path.join(
                    __dirname,
                    '..',
                    'assets/pdf/Formulario' +
                      data.primer_nombre +
                      '' +
                      data.apellido +
                      '.pdf'
                  ),
                  err => {
                    if (err) return console.error(err);
                  }
                );
                fs.remove(path.join(req.file.path), err => {
                  if (err) return console.error(err);
                });
                res.status(200).send({
                  status: true
                });
              }
            });
          }
        }
      );
  } catch (error) {
    console.log(error);
    res.status(500).send({
      error: error
    });
  }
}
async function SendPdfSolicitudDieta(req, res) {
  try {
    var data = req.body;
    console.log(req.query.dieta);
    var content = await PdfController.createSolicitud(data);
    pdf
      .create(content.content, content.options)
      .toFile(
        path.join(
          __dirname,
          '..',
          'assets/pdf/Formulario' +
            data.primer_nombre +
            '' +
            data.apellido +
            '.pdf'
        ),
        (err, response) => {
          if (err) {
            res.status(400).send({
              error: err
            });
          } else {
            var mailOptions = {
              from: 'info@fullmusculo.com',
              to:'info@fullmusculo.com;nutricionista@fullmusculo.com',
              /* to: 'utrera1695@gmail.com', */
              subject: 'Plan para ' + data.primer_nombre + ' ' + data.apellido,
              html:
                '<p><b>' +
                data.primer_nombre +
                ' ' +
                data.apellido +
                '</b> ha solicitado una plan personalizado de Dieta.</p>' +
                '<p>' +
                (req.query.dieta === 'true'
                  ? 'Además, seleccionó el plan de ejercicio adicional'
                  : '') +
                '</p>' +
                '<p>El correo de este usuario es ' +
                data.email +
                '</p>' +
                '</hr></hr>' +
                '<p>Adjunto está el formulario con los datos y la información del usuario.</p>',
              attachments: [
                {
                  filename:
                    'Formulario' +
                    data.primer_nombre +
                    '' +
                    data.apellido +
                    '.pdf',
                  path: path.join(
                    __dirname,
                    '..',
                    'assets/pdf/Formulario' +
                      data.primer_nombre +
                      '' +
                      data.apellido +
                      '.pdf'
                  ),
                  contentType: 'application/pdf'
                },
                {
                  filename: req.file.originalname,
                  path: path.join(req.file.path),
                  contentType: req.file.mimetype
                }
              ]
            };
            transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                console.log('error', error);
                res.status(500).send({
                  status: false
                });
              } else {
                console.log('bien');
                fs.remove(
                  path.join(
                    __dirname,
                    '..',
                    'assets/pdf/Formulario' +
                      data.primer_nombre +
                      '' +
                      data.apellido +
                      '.pdf'
                  ),
                  err => {
                    if (err) return console.error(err);
                  }
                );
                fs.remove(path.join(req.file.path), err => {
                  if (err) return console.error(err);
                });
                res.status(200).send({
                  status: true
                });
              }
            });
          }
        }
      );
  } catch (error) {
    console.log(error);
    res.status(500).send({
      error: error
    });
  }
}
module.exports = {
  DownloadPdfEjercicio,
  SendPdfEjercicio,
  SendPdfSolicitud,
  SendPdfSolicitudDieta
};
