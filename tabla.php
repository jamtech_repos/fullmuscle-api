<?php
//
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://fullmusculo.com/api/personal_routine/?id=".get_current_user_id(),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if ($err) {
    echo "cURL Error #:" . $err;
} else {
    $data = json_decode($response);
    $content = $data->content;
    $html = "<style>
        .content_day table {border-collapse: collapse;width: 100%; margin: 2rem auto}
        .content_day table td, table th {border: 1px solid #e5e5e5;text-align: center;vertical-align: center;}
        .content_day table tr.title th{background-color: #023aa2;font-weight: bold;font-size: 14px;}
        .content_day table th {background-color: #8e9ebd;color: white;text-transform: uppercase;padding: .5rem .2rem;border-color: #a0a2a4;font-size: 8px;}
        .content_day table tr {background-color: #f2f2f2;}
        .content_day table tr:nth-child(even){background-color: white;}
        .content_day table td.index {font-weight: bold;width: 20px;}
        /*table td.sets, table td.rpe, table td.carga, table td.descanso {width: 40px;}*/
        .content_day table td.notas {width: 180px;}
        .content_day table td {font-size: 8px}
    </style>";
		$html .= "<div>";
			foreach($content as $day) {
                $html .= "<div class=\"content_day\">";
                $html .= "<table>
							<thead>
							<tr class=\"title\">
								<th colspan='4'>Dia ".$day->id."</th>
							</tr>
							<tr>
								<th></th>
								<th>Ejercicio</th>
								<th>N Series</th>
								<th>Repeticiones</th>
							</tr>
							</thead>
							<tbody>";
                foreach($day->items as $exercise) {
                    $index = current($day);
                    $html .= "
							<tr>
							  <td class=\"index\">" . ($index + 1) . "</td>
							  <td style=\"text-align: start;margin: .5rem 0px\">
								<div>
									<b><a href=\"https://fullmusculo.com/ejercicios/#/ejercicio/" . $exercise->Exercise_Id . "\">" . $exercise->Exercise_Name_Complet . "</a></b>
									<p style=\"font-size: 6px; margin-bottom: 0px\"><b>Músculo:</b>" . $exercise->Target_Muscle_Group . "</p>
									<p style=\"font-size: 6px; margin-top: 0px\"><b>Implementos:</b>" . $exercise->Apparatus_Groups_Name . "</p>
								 </div>
							  </td>
							  <td class=\"sets\">" . $exercise->sets . "</td>
							  <td>" . $exercise->reps . "</td>
							</tr>
					";
				}
                $html .= "</tbody></table>";
                $html .= "</div>";
            }
		$html .= "</div>";

		//echo json_encode($data->content);
		echo $html;
	}
?>