'use strict';
const amazon = require('amazon-affiliate-api');
const RateLimiter = require('limiter').RateLimiter;
const limiter = new RateLimiter(1, 2000);
const Aws = require('../models/aws.model');
var moment = require('moment');

async function GetitemLookup(req, res) {
    const AWSAccessKeyId = 'AKIAJ7UWR6LI2CFKSUSA';
    const Secretkey = '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';
    const Tag = req.query.tag;
    const asin = req.query.asin;
    const domain = req.query.domain;
    console.log(domain);
    var result = await Aws.findOne({ asin: asin }).exec();
    console.log(result);
    if (result) {
        var today = new moment();
        console.log('diferencia ', today.diff(result.fecha, 'days'));
        if (today.diff(result.fecha, 'days') < -1) {
            limiter.removeTokens(1, async function() {
                var client = amazon.createClient({
                    awsId: AWSAccessKeyId,
                    awsSecret: Secretkey,
                    awsTag: Tag
                });
                client.itemLookup(
                    {
                        idType: 'ASIN',
                        itemId: asin,
                        responseGroup: 'Offers,Images',
                        domain: domain
                    },
                    async (err, response) => {
                        if (err) {
                            res.status(500).send({ error: err });
                        } else {
                            var data = response.Items.Item;
                            result.fecha = new moment().format('L');
                            result.image =
                                data[0].ImageSets.ImageSet[0].LargeImage.URL;
                            result.price =
                                data[0].OfferSummary.LowestNewPrice.FormattedPrice;
                            const update = await Aws.findOneAndUpdate(
                                { _id: result._id },
                                result
                            ).exec();
                            res.status(200).send({ data: update });
                        }
                    }
                );
            });
        } else {
            console.log('existente');
            res.status(200).send({ data: result });
        }
    } else {
        limiter.removeTokens(1, async function() {
            var client = amazon.createClient({
                awsId: AWSAccessKeyId,
                awsSecret: Secretkey,
                awsTag: Tag
            });
            client.itemLookup(
                {
                    idType: 'ASIN',
                    itemId: asin,
                    responseGroup: 'Offers,Images',
                    domain: domain
                },
                async (err, response) => {
                    if (err) {
                        console.log(JSON.stringify(err));
                        res.status(500).send({ error: err });
                    } else {
                        var data = response.Items.Item;
                        const aws = new Aws();
                        aws.fecha = new moment().format('L');
                        aws.image =
                            data[0].ImageSets.ImageSet[0].LargeImage.URL;

                        aws.price =
                            data[0].OfferSummary.LowestNewPrice.FormattedPrice;
                        aws.asin = asin;
                        await aws.save();
                        res.status(200).send({ data: aws });
                    }
                }
            );
        });
    }
}

module.exports = {
    GetitemLookup
};
