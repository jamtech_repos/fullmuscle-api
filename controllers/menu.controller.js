'use strict'

const Menu = require('../models/menu.model')

function getMenu(calorias,tipo) {
    Menu.findOne({calorias:calorias,tipo:tipo},(result) => {
        return result;
    })
}

function saveMenu(req,res) {
    var menu = new Menu(req.body)
    menu.save((result,err) => {
        if (err) {
            res.status(500).send('Error al guardar el menu',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo guardar el menu')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function listMenu(req,res) {
    Menu.find((result,err) => {
        if (err) {
            res.status(500).send('Error al listar los menus',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo listar los menus')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function updateMenu(req,res) {
    var update = req.body;
    var id = req.params.id;
    Menu.findOneAndUpdate(id,update,(result,err) => {
        if (err) {
            res.status(500).send('Error al actualizar el menu',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo actualizar el menu')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function deleteMenu(req,res) {
    var id = req.params.id;
    Menu.findByIdAndDelete(id,(result,err) => {
        if (err) {
            res.status(500).send('Error al eliminar el menu',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo eliminar el menu')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}


module.exports = {
    getMenu,
    saveMenu,
    listMenu,
    updateMenu,
    deleteMenu
}