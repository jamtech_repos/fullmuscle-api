'use strict'

const Recomendaciones = require('../models/recomendaciones.model')

function getRecomendacion(tipo) {
    var calorias;
    var tipo;
    Recomendaciones.findOne({calorias:calorias,tipo:tipo},(result) => {
        return result;
    })
}

function saveRecomendacion(req,res) {
    var reco = new Recomendaciones(req.body)
    reco.save((result,err) => {
        if (err) {
            res.status(500).send('Error al guardar el recomendaciones',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo guardar el recomendaciones')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function listRecomendaciones(req,res) {
    Recomendaciones.find((result,err) => {
        if (err) {
            res.status(500).send('Error al listar los recomendaciones',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo listar los recomendaciones')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function updateRecomendacion(req,res) {
    var update = req.body;
    var id = req.params.id;
    Recomendaciones.findOneAndUpdate(id,update,(result,err) => {
        if (err) {
            res.status(500).send('Error al actualizar el recomendaciones',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo actualizar el recomendaciones')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function deleteRecomendacion(req,res) {
    var id = req.params.id;
    Recomendaciones.findByIdAndDelete(id,(result,err) => {
        if (err) {
            res.status(500).send('Error al eliminar el recomendaciones',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo eliminar el recomendaciones')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}


module.exports = {
    getRecomendacion,
    saveRecomendacion,
    listRecomendaciones,
    updateRecomendacion,
    deleteRecomendacion
}