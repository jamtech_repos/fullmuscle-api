'use strict'

const Rutina = require('../models/rutina.model')

function getRutina(calorias,tipo) {
    Rutina.findOne({calorias:calorias,tipo:tipo},(result) => {
        return result;
    })
}

function saveRutina(req,res) {
    var rutina = new Rutina(req.body)
    rutina.save((result,err) => {
        if (err) {
            res.status(500).send('Error al guardar la Rutina',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo guardar la Rutina')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function listRutina(req,res) {
    Rutina.find((result,err) => {
        if (err) {
            res.status(500).send('Error al listar las Rutinas',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo listar las Rutinas')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function updateRutina(req,res) {
    var update = req.body;
    var id = req.params.id;
    Rutina.findOneAndUpdate(id,update,(result,err) => {
        if (err) {
            res.status(500).send('Error al actualizar la Rutina',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo actualizar la Rutina')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}

function deleteRutina(req,res) {
    var id = req.params.id;
    Rutina.findByIdAndDelete(id,(result,err) => {
        if (err) {
            res.status(500).send('Error al eliminar la Rutina',err)
        } else {
            if (!result) {
                res.status(400).send('No se pudo eliminar la Rutina')
            } else {
                res.status(200).send({result:result})
            }
        }
    })
}


module.exports = {
    getRutina,
    saveRutina,
    listRutina,
    updateRutina,
    deleteRutina
}