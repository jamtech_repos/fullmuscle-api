var pdf = require('html-pdf')
var fs = require('fs-extra')
const path = require('path')

var options = {
    "border": {
        "top": "1in",            // default is 0, units: mm, cm, in, px
        "right": "1in",
        "bottom": "1in",
        "left": "1in"
    },
    "format": "Letter",     
}

function createPDFUser(data) {
    var contenido = `
    <div class="container">
    <div class="row">
        <div>
            <p><strong>Nombre: </strong></p>
            <p><strong>Edad: </strong></p>
            <p><strong>Sexo: </strong></p>
            <p><strong>Altura: </strong></p>
            <p><strong>Peso: </strong></p>
        </div>
        <div>
            <p><strong>IMC: </strong></p>
            <p><strong>Peso normal</strong></p>
            <p><strong>Peso ideal: </strong></p>
        </div>
    </div>
</div>
    `
}

function createPDFAdmin(data) {
    var contenido = '<h1>Datos Seleccionados</h1>' +
        '<p><b>Nombre:</b> ' + data.seleccion.name + '</p>' +
        '<p><b>Correo:</b> ' + data.seleccion.email + '</p>' +
        '<p><b>Sexo:</b> ' + data.seleccion.sexo + '</p>' +
        '<p><b>Edad:</b> ' + data.seleccion.edad + '</p>' +
        '<p><b>Peso en Kg:</b> ' + ((data.seleccion.tipo === 1)? data.seleccion.peso:(data.seleccion.peso/2.2046)) + '<p>' +
        '<p><b>Altura en cm:</b> ' + ((data.seleccion.tipo === 1)? data.seleccion.altura:(data.seleccion.altura*2.54)) + '<p>' +
        '<p><b>Actividad Física:</b> ' + getNivel(data.seleccion.actividad_fisica) + '</p>' +
        '<p><b>Carnes:</b> ' + getCarnes(data.seleccion.carnes) + '</p>' +
        '<p><b>Alergias y/o intolerancias:</b> ' + getAlergias(data.seleccion.alergias) + '</p>' +
        '<p><b>Malos habitos:</b> ' + getMalosHabitos(data.seleccion.malos_habitos) + '</p>' +
        '<p><b>Objetivo:</b>' + getObjetivo(data.seleccion.objetivo) + '</p>' +

        '<h1>Datos calculados</h1>' + 
        '<p><b>BMI:</b> ' + data.seleccion.bmi + '</p>' +
        '<p><b>Peso Ideal:</b> ' + data.seleccion.peso_ideal_kg + '</p>' +
        '<p><b>Calorias para el objetivo:</b> ' + data.seleccion.calorias_objetivo + '</p>' +
        '<p><b>Calorias por el nivel de actividad física:</b> ' + data.seleccion.calorias_nivel + '</p>' +

        '<h2>Macronutrientes recomendados</h2>' +
        '<p><b>Proteinas por dia:</b> ' + data.seleccion.proteinas_dia_g + '</p>' +
        '<p><b>Grasa por día:</b> ' + data.seleccion.grasa_dia_g + '</p>' +
        '<p><b>Carbohidratos por día:</b> ' + data.seleccion.carbohidratos_dia_g + '</p>' +
    
        '<h2>Maximo potencial muscular</h2>' +
        '<p><b>Valor superior:</b> ' + ((data.seleccion.sexo === 'hombre') ? data.seleccion.mpm_superior: 'No aplica') + '</p>' +
        '<p><b>Valor inferior:</b> ' + ((data.seleccion.sexo === 'hombre') ? data.seleccion.mpm_inferior: 'No aplica') + '</p>' +

        '<h1>Datos ingresados en el formulario</h1>' +
        '<p><b>Nombre:</b> ' + data.formulario.nombre + '</p>' +
        '<p><b>Apellido:</b> ' + data.formulario.apellido + '</p>' +
        '<p><b>Email:</b> ' + data.formulario.email + '</p>' +
        '<p><b>¿Como te enteraste de nuestra promoción para dieta personalizada?:</b> ' + data.formulario.como_t_enteraste + '</p>' +
        '<p><b>Si hiciste el pago desde la cuenta paypal de alguien más indícanos su nombre e email>:</b> ' + data.formulario.nombre_dueno_paypal + '</p>' +
        '<p><b>País de donde nos escribe:</b> ' + data.formulario.pais + '</p>' +
        '<p><b>Peso mínimo y máximo que has presentado:</b> ' + data.formulario.peso_min_y_max + '</p>' +
        '<p><b>Medidas de cintura y cadera en cm:</b> ' + data.formulario.cintura_cadera + '</p>' +
        '<p><b>Porcentaje de grasa corporal (si lo sabe actualizado) y ¿cómo fue medido?:</b> ' + data.formulario.por_grasa_corporal + '</p>' +
        '<p><b>Cantidad de agua que toma al día (En litros o vasos de agua):</b> ' + data.formulario.cantidad_agua + '</p>' +
        '<p><b>¿Toma café o te regularmente?:</b> ' + getCafeTe(data.formulario.cafe_te) + '</p>' +
        '<p><b>¿Cual es la frecuencia de evacuaciones?, ¿Cada cuanto va al baño?, ¿Sufre de estreñimiento?:</b> ' + data.formulario.evacuaciones + '</p>' +
        '<p><b>¿Sientes ansiedad en algún momento del día?, ¿A qué hora?:</b> ' + data.formulario.ansiedad + '</p>' +
        '<p><b>¿Has entrenado anteriormente?:</b> ' + data.formulario.entrenamiento + '</p>' +
        '<p><b>Si la anterior pregunta fue si, ¿Desde cuando entrenas?:</b> ' + data.formulario.desde_entrenamiento + '</p>' +
        '<p><b>¿Que tipo de entrenamiento realizas?, Ser especifico:</b> ' + data.formulario.tipo_entrenamiento + '</p>' +
        '<p><b>¿A qué hora del día entrenas normalmente?:</b>' + data.formulario.hora_entrenamiento + '</p>' +
        '<p><b>¿Cuantos días a la semana entrenas o dispones para entrenar?:</b>' + data.formulario.dias_entrenamiento + '</p>' + 
        '<p><b>¿De cuanto tiempo dispones al día para entrenar?:</b> ' + data.formulario.tiempo_entrenamiento + '</p>' +
        '<p><b>¿Objetivos y expectativas?:</b> ' + data.formulario.objetivo + '</p>' +
        '<p><b>Explicar con detalle cual es el objetivo que quieres lograr:</b> ' + data.formulario.objetivo_explicado + '</p>' +
        '<p><b>¿Cuantas comidas hace al día y en que horarios?:</b> ' + data.formulario.comidas_dia + '</p>' +
        '<p><b>Frutas y vegetales que comúnmente consume:</b> ' + data.formulario.frutas_vegetales + '</p>' +
        '<p><b>Alimentos que nunca consume porque no te gustan:</b> ' + data.formulario.alimentos_no_consume + '</p>' +
        '<p><b>Desayuno:</b>' + data.formulario.desayuno + '</p>' +
        '<p><b>Merienda de media mañana:</b> ' + data.formulario.merienda_manana + '</p>' +
        '<p><b>Almuerzo:</b> ' + data.formulario.almuerzo + '</p>' +
        '<p><b>Merienda de media tarde:</b> ' + data.formulario.merienda_tarde + '</p>' +
        '<p><b>Cena:</b> ' + data.formulario.cena + '</p>' +
        '<p><b>Merienda antes de dormir:</b> ' + data.formulario.merienda_noche + '</p>' +
        '<p><b>¿Has tomado o tomas suplementos?:</b> ' + data.formulario.suplementos + '</p>' +
        '<p><b>Si la anterior respuesta es si, ¿cuales suplementos has tomado o tomas actualmente?:</b> ' + data.formulario.cuales_suplementos + '</p>' +
        '<p><b>¿Alergias/Intolerancias/Enfermedades/Medicación? sEspecificar:</b> ' + data.formulario.alergias + '</p>' +
        '<p><b>¿Lesiones o impedimentos para realizar algún ejercicio?:</b> ' + data.formulario.lesiones + '</p>' +
        '<p><b>Comentarios adicionales que desees agregar:</b> ' + data.formulario.comentarios + '</p>' +
        '<p><b>Whatsapp:</b> ' + data.formulario.whatsapp + '</p>'

    return {contenido:contenido,options:options};
}

function getCarnes(carnes) {
    var resultado = '';
    for (let i = 0; i < carnes.length; i++) {
        if (carnes[i].status) {
            resultado += carnes[i].name + ';';
        } 
    }
    return resultado;
}

function getAlergias(alergias) {
    var resultado = '';
    for (let i = 0; i < alergias.length; i++) {
        if (alergias[i].status) {
            resultado += alergias[i].name + ';'
        }
    }
    return resultado;
}

function getMalosHabitos(habitos) {
    var resultado = '';
    for (let i = 0; i < habitos.length; i++) {
        if (habitos[i].status) {
            resultado += habitos[i].name + ';'
        }
    }    
    return resultado;
}

function getCafeTe(cafete) {
    var resultado = '';
    for (let i = 0; i < cafete.length; i++) {
        if (cafete[i].status) {
            resultado += cafete[i].value + ';'
        }
    }
    return resultado;
}

function getNivel(nivel) {
    switch (nivel) {
      case '1': return 'Sedentario';
      case '2': return 'Ejercicio Leve';
      case '3': return 'Ejercicio Moderado';
      case '4': return 'Ejercicio Duro';
      case '5': return 'Atleta';
    }
}

function getObjetivo(objetivo) {
    switch (objetivo) {
      case '1': return 'Aumentar masa muscular';
      case '2': return 'Mantenimiento';
      case '3': return 'Perder grasa corporal';
    }
}
module.exports = {
    createPDFAdmin,
    createPDFUser
}