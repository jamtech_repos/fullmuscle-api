'use strict';
var Datos = require('../models/datos.model');
var recomendacionesController = require('./recomendaciones.controller');
var transporter = require('../mail');
var pdfcontroller = require('./pdf.controller');
var fs = require('fs-extra');
var pdf = require('html-pdf');

const path = require('path');

function calculateResult(req, res) {
  var params = req.body;
  var datos = new Datos(params);
  //CALCULAR BMR
  let peso_kg = datos.tipo === 1 ? datos.peso : datos.peso / 2.2046;
  let altura_cm = datos.tipo === 1 ? datos.altura : datos.altura * 2.54;
  let valor_sexo_1 = datos.sexo === 'hombre' ? 5 : -161;
  datos.bmr = Math.round(
    9.99 * peso_kg + 6.25 * altura_cm - 4.92 * datos.edad + valor_sexo_1
  );

  //CALCULAR CALORIAS SEGUN EL NIVEL DE ACTIVIDAD FISICA
  datos.calorias_nivel = Math.round(
    datos.bmr * getActividadFisica(datos.actividad_fisica)
  );

  //CALCULAR BMI/IMC9
  let altura_mts = datos.tipo === 1 ? datos.altura / 100 : datos.altura / 39.37;
  datos.bmi = (peso_kg / (altura_mts * altura_mts)).toFixed(1);

  //CALCULAR PESO IDEAL
  let altura_in = datos.tipo === 1 ? datos.altura / 2.54 : datos.altura;
  let valor_sexo_2 = datos.sexo === 'hombre' ? 52 : 49;
  let valor_sexo_3 = datos.sexo === 'hombre' ? 1.9 : 1.7;
  let altura_60 = altura_in > 60 ? altura_in - 60 : 60 - altura_in;
  datos.peso_ideal_kg = Math.round(valor_sexo_2 + valor_sexo_3 * altura_60);

  //CALCULAR MPM
  datos.mpm_superior = (altura_cm - 98).toFixed(1);
  datos.mpm_inferior = (altura_cm - 102).toFixed(1);

  //CALCULAR CALORIAS SEGUN OBJETIVO
  datos.calorias_objetivo = Math.round(
    getCaloriasObjetivo(datos.calorias_nivel, datos.objetivo)
  );

  //CALCULAR MACRONUTRIENTES
  datos.proteinas_dia_g = Math.round((datos.calorias_objetivo * 0.3) / 4);
  datos.grasa_dia_g = Math.round((datos.calorias_objetivo * 0.35) / 9);
  datos.carbohidratos_dia_g = Math.round((datos.calorias_objetivo * 0.35) / 4);

  //BUSCAMOS RECOMENDACIONES
  var recomendaciones = [];
  for (let i = 0; i < datos.malos_habitos.length - 1; i++) {
    if (!datos.malos_habitos[5].status) {
      if (datos.malos_habitos[i].status) {
        recomendaciones.push(recomendacionesController.getRecomendacion(i));
      }
    } else {
      break;
    }
  }
  res.status(200).send({
    result: datos,
    recomendaciones: recomendaciones
  });
  /* datos.save((err,result) => {
        if (err) {
            res.status(500).send('Error en la solicitud')
        } else {
            if (result) {
                res.status(200).send({result: result,recomendaciones:recomendaciones})
            }
        }
    }) */
}

function sendPDFToUser(req, res) {
  var data = req.body;
  // Definimos el email

  var contenido = `
    <h1>Test para html-pdf</h1>
    <p>Estamos creando un archivo PDF con este código HTML sencillo</p>`;
  pdf
    .create(contenido)
    .toFile('../assets/pdf/Planfullmusculo.pdf', function (err, res) {
      if (err) {
        console.log(err);
      } else {
        var mailOptions = {
          from: 'recuperacion@getxplor.com',
          to: data.email + '',
          subject: 'Plan de Dieta personalizada',
          text: 'Feliz día ' +
            data.nombre +
            '.\n\n' +
            'Adjunto tu plan de alimentación y te damos a continuación unos detalles a tomar en cuenta.\n\n' +
            'El peso ideal es un promedio, un estimado que se calcula en base en tu altura. No debes tomarlo muy en serio' +
            'sobre todo si haces ejercicios con pesas. Al igual que el IMC son números referenciales.\n' +
            'Esto debido a que pudiera existir el caso en el que te encuentres por encima de tu peso ideal pero luzcas' +
            'mejor que nunca con un porcentaje de grasa corporal bajo.\n\n' +
            'Ten en cuenta que la mejor forma de registrar tu progreso es hacerte medir la grasa corporal o con fotos' +
            'frente al espejo.\n\n' +
            'Evita los aderezos en las ensaladas y salsas en las carnes ya que agregan muchas calorías extras que no' +
            'nos aportan mucho valor nutricional.\n\n' +
            'También debes evitar las calorías líquidas sin ningún valor nutricional que aportan las bebidas azúcaradas,' +
            'gaseosas y procesadas. Lo mejor es el agua.\n\n\n' +
            'Saludos.' +
            'www.fullmusculo.com',
          attachments: [{
            filename: 'Planfullmusculo.pdf',
            path: path.join(__dirname, '../assets/pdf/Planfullmusculo.pdf'),
            contentType: 'application/pdf'
          }]
        }; // Enviamos el email
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            return error;
          } else {
            return info;
          }
        });
      }
    });
}

function sendDataToNutricionista(req, res) {
  const data = req.body;
  const pdfdatos = pdfcontroller.createPDFAdmin(data);
  pdf
    .create(pdfdatos.contenido, pdfdatos.options)
    .toFile(
      path.join(
        __dirname,
        '..',
        'assets/pdf/Formulario' +
        data.formulario.nombre +
        '' +
        data.formulario.apellido +
        '.pdf'
      ),
      (err, response) => {
        if (err) {
          console.log('error al crear pdf', err);
          res.status(500).send({
            status: false
          });
        } else {
          var mailOptions = {
            from: 'info@fullmusculo.com',
            to: 'info@fullmusculo.com;nutricionista@fullmusculo.com',
            subject: 'Plan de dieta personalizada para ' +
              data.formulario.nombre +
              ' ' +
              data.formulario.apellido,
            html: '<p><b>' +
              data.formulario.nombre +
              ' ' +
              data.formulario.apellido +
              '</b> ha solicitado una dieta personalizada.</p>' +
              '<p>' +
              (data.seleccion.ejercicio ?
                'Además, seleccionó el plan de ejercicio adicional de ' +
                data.seleccion.tipo_ejercicio +
                ' días' :
                '') +
              '</p>' +
              '<p>El correo de este usuario es ' +
              data.seleccion.email +
              '</p>' +
              '</hr></hr>' +
              '<p>Adjunto está el formulario con los datos y la información del usuario.</p>',
            attachments: [{
              filename: 'Formulario' +
                data.formulario.nombre +
                '' +
                data.formulario.apellido +
                '.pdf',
              path: path.join(
                __dirname,
                '..',
                'assets/pdf/Formulario' +
                data.formulario.nombre +
                '' +
                data.formulario.apellido +
                '.pdf'
              ),
              contentType: 'application/pdf'
            }]
          };
          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
              console.log('error', error);
              res.status(500).send({
                status: false
              });
            } else {
              console.log('bien');
              fs.remove(
                path.join(
                  __dirname,
                  '..',
                  'assets/pdf/Formulario' +
                  data.formulario.nombre +
                  '' +
                  data.formulario.apellido +
                  '.pdf',
                  err => {
                    if (err) return console.error(err);
                    res.status(200).send({
                      status: true
                    });
                  }
                )
              );
            }
          });
        }
      }
    );
}

function getActividadFisica(value) {
  switch (value) {
    case '1':
      return 1.2;
    case '2':
      return 1.37;
    case '3':
      return 1.55;
    case '4':
      return 1.73;
    case '5':
      return 1.9;
  }
}

function getCaloriasObjetivo(calorias_nivel, objetivo) {
  switch (objetivo) {
    case '1':
      return calorias_nivel * 1.2;
    case '2':
      return calorias_nivel;
    case '3':
      return calorias_nivel * 0.8;
  }
}

module.exports = {
  calculateResult,
  sendPDFToUser,
  sendDataToNutricionista
};