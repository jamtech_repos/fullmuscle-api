"use strict";

var express = require("express");
var personalRoutineController = require("./personalRoutine.controller");
var api = express.Router();

api.post("/personal_routine", personalRoutineController.saveRoutine);
api.get("/personal_routine", personalRoutineController.getRoutine);

module.exports = api;
