"use stric";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var RutinaSchema = Schema({
  user: String,
  content: Object,
});

module.exports = mongoose.model("PersonalRutine", RutinaSchema);
