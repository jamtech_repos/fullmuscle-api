"use strict";
const personalRoutine = require("./personalRoutine.model");

async function saveRoutine(req, res) {
  try {
    var data = req.body.data;
    console.log(data);
    personalRoutine.findOne({ user: data.user }, (err, result) => {
      if (err) {
        res.send(500, { text: "Error al guardar la Rutina", err: err });
      } else if (result) {
        console.log(result)
        personalRoutine.findOneAndUpdate({_id: result._id}, data, (err, updateResult) => {
          if (err) {
            res.send(500, { text: "Error al actualizar la Rutina", err });
          } else {
            if (!result) {
              res.send(400, "No se pudo actualizar la Rutina");
            } else {
              res.send(200, { result: data });
            }
          }
        });
      } else {
        const newRoutine = new personalRoutine(data);
        newRoutine.save((err, result) => {
          if (err) {
            res.send(500, "Error al guardar la Rutina", err);
          } else {
            if (!result) {
              res.send(400, "No se pudo guardar la Rutina");
            } else {
              res.send(200, { result: result });
            }
          }
        });
      }
    });
  } catch (error) {
    console.log(error);
    res.send(500, {
      error: error,
    });
  }
}

async function getRoutine(req, res) {
  try {
    var id = req.query.id;
    console.log(req);
    personalRoutine.findOne({ user: id }, (err, result) => {
      if (err) {
        res.send(500, { text: "Error al guardar la Rutina", err });
      } else {
        if (!result) {
          res.send(400, "No se pudo encontrar la Rutina");
        } else {
          res.send(200, result);
        }
      }
    });
  } catch (error) {
    console.log(error);
    res.send(500, {
      error: error,
    });
  }
}

module.exports = {
  saveRoutine,
  getRoutine,
};
