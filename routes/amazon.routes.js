'use strict';

var express = require('express');
var AmazonController = require('../controllers/amazon.controller');
var api = express.Router();

api.get('/aws', AmazonController.GetitemLookup);

module.exports = api;
