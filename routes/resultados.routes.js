'use strict'

var express = require('express')
var ResultadosController = require('../controllers/resultados.controller')
var api = express.Router()

api.post('/calculo',ResultadosController.calculateResult);
api.post('/sendpdf',ResultadosController.sendPDFToUser)
api.post('/sendadminemail',ResultadosController.sendDataToNutricionista)
module.exports = api;