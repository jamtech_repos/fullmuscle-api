'use strict'

var express = require('express')
var EjercicioController = require('../controllers/ejercicios.controller')
var api = express.Router()

api.post('/ejercicio',EjercicioController.saveRutina);
api.get('/ejercicio',EjercicioController.listRutina);
api.put('/ejercicio/:id',EjercicioController.updateRutina)
api.delete('/ejercicio/:id',EjercicioController.deleteRutina)

module.exports = api;