'use strict'

var express = require('express')
var RecomendacionesController = require('../controllers/recomendaciones.controller')
var api = express.Router()

api.post('/recomendacion',RecomendacionesController.saveRecomendacion);
api.get('/recomendacion',RecomendacionesController.listRecomendaciones);
api.put('/recomendacion/:id',RecomendacionesController.updateRecomendacion)
api.delete('/recomendacion/:id',RecomendacionesController.deleteRecomendacion)

module.exports = api;