'use strict'

var express = require('express')
var MenuController = require('../controllers/menu.controller')
var api = express.Router()

api.post('/menu',MenuController.saveMenu);
api.get('/menu',MenuController.listMenu);
api.put('/menu/:id',MenuController.updateMenu)
api.delete('/menu/:id',MenuController.deleteMenu)

module.exports = api;