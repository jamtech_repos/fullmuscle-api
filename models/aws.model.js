'use stric';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AwsSchema = Schema({
    image: String,
    price: String,
    asin: String,
    fecha: Date
});

module.exports = mongoose.model('Aws', AwsSchema);
