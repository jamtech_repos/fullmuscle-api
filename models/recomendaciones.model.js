'use stric'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RecomendacionSchema = Schema({
    tipo: String,
    titulo: String,
    contenido: String
})

module.exports = mongoose.model('Recomendacion', RecomendacionSchema);