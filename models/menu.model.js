'use stric'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MenuSchema = Schema({
    tipo: String,
    calorias: String,
    contenido: String
})

module.exports = mongoose.model('Menu', MenuSchema);