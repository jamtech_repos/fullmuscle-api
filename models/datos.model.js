'use stric'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DatosSchema = Schema({
    //opciones ingresadas
    sexo: String,
    actividad_fisica: String,
    carnes: Array,
    alergias: Array,
    objetivo: String,
    malos_habitos: Array,
    tipo: Number,
    edad: Number,
    altura: Number,
    peso: Number,

    //datos de usuario
    email: String,
    name: String,

    ejercicio: Boolean,
    tipo_ejercicio: Number,
    
    //calculos
    bmr: Number,
    calorias_nivel: Number,
    bmi: Number,
    peso_ideal_kg: Number,
    mpm_superior: Number,
    mpm_inferior: Number,
    calorias_objetivo: Number,
    proteinas_dia_g: Number,
    grasa_dia_g: Number,
    carbohidratos_dia_g: Number,

})

module.exports = mongoose.model('Datos', DatosSchema);