"use strict";

const csp = require("content-security-policy");
const express = require("express");
const bodyParser = require("body-parser");
var cors = require("cors");
var multer = require("multer");
var upload = multer();
var app = express();

//CARGAR RUTAS
var ResultadosRoutes = require("./routes/resultados.routes");
var MenuRoutes = require("./routes/menu.routes");
var EjercicioRoutes = require("./routes/ejercicios.routes");
var RecomendacionesRoutes = require("./routes/recomendaciones.routes");
var AmazonRoutes = require("./routes/amazon.routes");

var EjerciciosSectionRoutes = require("./ejercicios/ejercicios.routes");
var personalRoutineRoutes = require("./personalRoutine/personalRoutine.routes");

const globalCSP = csp.getCSP(csp.STARTER_OPTIONS); /* 
app.use(upload.array()) */
// This will apply this policy to all requests if no local policy is set
app.use(globalCSP);
app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());
//configurar cabeceras http
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  res.header('Cache-Control', 'no-store')
  next();
});

app.use("/api", ResultadosRoutes);
app.use("/api", MenuRoutes);
app.use("/api", EjercicioRoutes);
app.use("/api", RecomendacionesRoutes);
app.use("/api", AmazonRoutes);
app.use("/api", EjerciciosSectionRoutes);
app.use("/api", personalRoutineRoutes);
app.use("/api", function (req, res) {
  res.status(200).send({ done: true });
});
module.exports = app;
