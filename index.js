'use strict';

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 8000;
var https = require('https');
var fs = require('fs-extra');
//CONEXION A LA BASE DE DATOS
mongoose.connect(
    'mongodb://localhost/fullmusculo',
    { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true  },
    (err, res) => {
        if (err) {
            throw err;
        } else {
            console.log('Conexion a la base de datos establecida');
            app.listen(port, function() {
                console.log(
                    'Servidor escuchando por el puerto http://localhost:' + port
                );
            });
        }
    }
);
